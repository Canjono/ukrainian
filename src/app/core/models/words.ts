export interface Word {
  id: string
  ukrainian: string
  translation: string
  story: string
  createdAt: string
  updatedAt: string
}
