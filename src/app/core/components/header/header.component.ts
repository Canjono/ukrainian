import { ChangeDetectionStrategy, Component, inject } from '@angular/core'
import { FirebaseService } from 'src/app/core/services/firebase.service'
import { HamburgerMenuComponent } from './hamburger-menu/hamburger-menu.component'
import { RouterModule } from '@angular/router'

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [HamburgerMenuComponent, RouterModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  private _firebaseService = inject(FirebaseService)

  isSignedIn = this._firebaseService.isSignedIn

  logOut(): void {
    this._firebaseService.logout()
  }
}
