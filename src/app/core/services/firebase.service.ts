import { inject, Injectable, signal } from '@angular/core'

import { initializeApp } from 'firebase/app'
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged, Auth, signOut } from 'firebase/auth'
import { environment } from 'src/environments/environment'
import { Router } from '@angular/router'
import { getDatabase, ref, set, onValue } from 'firebase/database'
import { Observable } from 'rxjs'
import { MatSnackBar } from '@angular/material/snack-bar'
import { FirebaseKey } from '../models/firebase'

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  #router = inject(Router)
  #snackbar = inject(MatSnackBar)

  #auth!: Auth
  #database!: any

  isSignedIn = signal<boolean>(false)

  constructor() {
    const firebaseConfig = environment.firebaseConfig

    const app = initializeApp(firebaseConfig)
    this.#database = getDatabase(app)

    this.#initAuth()
  }

  #initAuth(): void {
    this.#auth = getAuth()

    onAuthStateChanged(this.#auth, (user) => {
      this.isSignedIn.set(!!user)
    })
  }

  // #readWords(): any {
  //   const wordsRef = ref(this.#database, FirebaseKey.Words)

  //   onValue(wordsRef, (snapshot) => {
  //     const words = snapshot.val() ?? []
  //     this.words.set(words)
  //   })
  // }

  signIn(email: string, password: string): void {
    signInWithEmailAndPassword(this.#auth, email, password)
      .then((_) => {
        this.isSignedIn.set(true)
        this.#router.navigate(['/'])
      })
      .catch((error) => {
        this.#snackbar.open(`${error.code}`, 'OK')
      })
  }

  logout(): void {
    signOut(this.#auth)
      .then(() => {
        this.isSignedIn.set(false)
        this.#router.navigate(['/sign-in'])
      })
      .catch((error) => {
        this.#snackbar.open(`${error.code}`, 'OK')
      })
  }

  // writeWord(newWord: Word): Observable<void> {
  //   return new Observable((subscriber) => {
  //     set(ref(this.#database, FirebaseKey.Words), [...this.words(), newWord])
  //       .then(() => {
  //         subscriber.next()
  //       })
  //       .catch((error) => {
  //         subscriber.error(error)
  //       })
  //   })
  // }

  // updateWords(): Observable<void> {
  //   return new Observable((subscriber) => {
  //     set(ref(this.#database, FirebaseKey.Words), this.words())
  //       .then(() => {
  //         subscriber.next()
  //       })
  //       .catch((error) => {
  //         subscriber.error(error)
  //       })
  //   })
  // }

  read<T>(key: FirebaseKey): Observable<T[]> {
    return new Observable((observer) => {
      const dbRef = ref(this.#database, key)

      onValue(dbRef, (snapshot) => {
        // const value = snapshot.val() ?? []
        const value =
          key === FirebaseKey.Dialogs
            ? <T[]>[{ id: 1, title: 'Presentation', content: 'This is the content' }]
            : snapshot.val() ?? []
        observer.next(value)
        observer.complete()
      })
    })
  }

  set(key: FirebaseKey, value: any): Observable<void> {
    return new Observable((observer) => {
      set(ref(this.#database, key), value)
        .then(() => {
          observer.next()
          observer.complete()
        })
        .catch(() => {
          observer.error()
        })
    })
  }

  // deleteWord(id: string): void {
  //   const filteredWords = this.words().filter((word) => word.id !== id)
  //   set(ref(this.#database, FirebaseKey.Words), filteredWords)
  // }
}
