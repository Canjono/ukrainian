import { ChangeDetectionStrategy, Component, inject, Input, OnInit } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { WordService } from 'src/app/core/services/word.service'

@Component({
  selector: 'app-update-word',
  standalone: true,
  imports: [MatButtonModule, ReactiveFormsModule],
  templateUrl: './update-word.component.html',
  styleUrls: ['./update-word.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateWordComponent implements OnInit {
  private _wordService = inject(WordService)

  @Input() id!: string

  form = this._wordService.updateWordForm

  ngOnInit(): void {
    this._wordService.initUpdateWord(this.id)
  }

  update(): void {
    this._wordService.updateWord(this.id)
  }
}
