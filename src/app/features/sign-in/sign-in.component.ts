import { Component, effect, inject } from '@angular/core'
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { Router } from '@angular/router'
import { FirebaseService } from 'src/app/core/services/firebase.service'

@Component({
  selector: 'app-sign-in',
  standalone: true,
  imports: [MatButtonModule, ReactiveFormsModule],
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {
  private _formBuilder = inject(FormBuilder)
  private _firebaseService = inject(FirebaseService)
  private _router = inject(Router)

  form = this._formBuilder.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  })

  constructor() {
    this._listenForSignIn()
  }

  private _listenForSignIn(): void {
    effect(() => {
      if (this._firebaseService.isSignedIn()) {
        this._router.navigate(['/'])
      }
    })
  }

  onSubmit(): void {
    if (this.form.invalid) {
      return
    }

    const email = this.form.controls.email.value
    const password = this.form.controls.password.value

    this._firebaseService.signIn(email!, password!)
  }
}
