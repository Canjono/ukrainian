import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatMenuModule } from '@angular/material/menu'

@Component({
  selector: 'app-more-options',
  standalone: true,
  imports: [MatButtonModule, MatIconModule, MatMenuModule],
  templateUrl: './more-options.component.html',
  styleUrls: ['./more-options.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoreOptionsComponent implements OnInit {
  @Input() showUkrainian!: boolean
  @Input() showTranslations!: boolean

  @Output() toggleUkrainian = new EventEmitter<void>()
  @Output() toggleTranslations = new EventEmitter<void>()
  @Output() toggleEditMode = new EventEmitter<void>()

  ngOnInit(): void {}

  onToggleUkrainian(): void {
    this.toggleUkrainian.emit()
  }

  onToggleTranslations(): void {
    this.toggleTranslations.emit()
  }

  onToggleEditMode(): void {
    this.toggleEditMode.emit()
  }
}
