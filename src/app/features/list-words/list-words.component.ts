import { ChangeDetectionStrategy, Component, inject } from '@angular/core'
import { WordService } from 'src/app/core/services/word.service'
import { MoreOptionsComponent } from './more-options/more-options.component'
import { WordCardComponent } from './word-card/word-card.component'

@Component({
  selector: 'app-list-words',
  standalone: true,
  imports: [MoreOptionsComponent, WordCardComponent],
  templateUrl: './list-words.component.html',
  styleUrls: ['./list-words.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListWordsComponent {
  #wordService = inject(WordService)

  showUkrainian = false
  showTranslations = false
  inEditMode = false
  showMoreOptions = false

  words = this.#wordService.words

  toggleUkrainian(): void {
    this.showUkrainian = !this.showUkrainian
  }

  toggleTranslations(): void {
    this.showTranslations = !this.showTranslations
  }

  toggleEditMode(): void {
    this.inEditMode = !this.inEditMode
  }

  toggleMoreOptions(): void {
    this.showMoreOptions = !this.showMoreOptions
  }
}
