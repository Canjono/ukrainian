import { ChangeDetectionStrategy, Component, inject, Input, OnInit } from '@angular/core'
import { Word } from 'src/app/core/models/words'
import { WordService } from 'src/app/core/services/word.service'
import { WordDialogComponent } from 'src/app/features/list-words/word-dialog/word-dialog.component'
import { MatDialog } from '@angular/material/dialog'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { RouterModule } from '@angular/router'

@Component({
  selector: 'app-word-card',
  standalone: true,
  imports: [MatButtonModule, MatIconModule, RouterModule],
  templateUrl: './word-card.component.html',
  styleUrls: ['./word-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WordCardComponent implements OnInit {
  #wordService = inject(WordService)
  #dialog = inject(MatDialog)

  @Input() word!: Word
  @Input() showUkrainian!: boolean
  @Input() showTranslations!: boolean
  @Input() inEditMode!: boolean

  ngOnInit(): void {}

  delete(): void {
    this.#wordService.delete(this.word)
  }

  openDialog(): void {
    this.#dialog.open(WordDialogComponent, {
      data: { word: this.word },
      minWidth: 300,
    })
  }
}
