export interface Dialog {
  id: string
  title: string
  content: string
}
