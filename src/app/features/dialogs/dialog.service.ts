import { Injectable, inject } from '@angular/core'
import { FirebaseService } from 'src/app/core/services/firebase.service'
import { Dialog } from './models/dialog'
import { Observable } from 'rxjs'
import { FirebaseKey } from 'src/app/core/models/firebase'

@Injectable()
export class DialogService {
  #firebaseService = inject(FirebaseService)

  dialogs: Observable<Dialog[]> = this.#firebaseService.read<Dialog>(FirebaseKey.Dialogs)
}
