import { Component, inject } from '@angular/core'
import { Dialog } from './models/dialog'
import { DialogService } from './dialog.service'
import { Observable } from 'rxjs'
import { CommonModule } from '@angular/common'

@Component({
  selector: 'app-dialogs',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './dialogs.component.html',
  styleUrl: './dialogs.component.scss',
  providers: [DialogService],
})
export class DialogsComponent {
  #dialogsService = inject(DialogService)

  dialogs = this.#dialogsService.dialogs

  // get dialogs(): Observable<Dialog[]> {
  //   return this.#dialogsService.dialogs
  // }
}
