import { ChangeDetectionStrategy, Component, inject, OnInit } from '@angular/core'
import { FormGroup, ReactiveFormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { WordService } from 'src/app/core/services/word.service'

@Component({
  selector: 'app-add-word',
  standalone: true,
  imports: [ReactiveFormsModule, MatButtonModule],
  templateUrl: './add-word.component.html',
  styleUrls: ['./add-word.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddWordComponent implements OnInit {
  private _wordService = inject(WordService)

  form = this._wordService.addWordForm as FormGroup

  ngOnInit(): void {}

  add(): void {
    this._wordService.add()
  }
}
