import { Word } from '../../../core/models/words'

export class Flashcard {
  guessWord!: string
  solutionWord!: string
  story!: string | null
  currentSide = FlashcardSide.Front

  constructor(word: Word, language: string) {
    switch (language) {
      case 'ukrainian':
        this.guessWord = word.ukrainian
        this.solutionWord = word.translation
        break
      case 'english':
        this.guessWord = word.translation
        this.solutionWord = word.ukrainian
    }

    this.story = word.story
  }

  turnAround(): void {
    switch (this.currentSide) {
      case FlashcardSide.Front:
        this.currentSide = FlashcardSide.Back
        break
      case FlashcardSide.Back:
        this.currentSide = FlashcardSide.Front
        break
    }
  }
}

export enum FlashcardSide {
  Front,
  Back,
}
