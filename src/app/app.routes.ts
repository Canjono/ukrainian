import { Routes } from '@angular/router'
import { ListWordsComponent } from './features/list-words/list-words.component'
import { SignInComponent } from './features/sign-in/sign-in.component'
import { AuthGuardService } from './core/services/auth-guard.service'

export const routes: Routes = [
  { path: 'sign-in', component: SignInComponent },
  {
    path: 'list-words',
    component: ListWordsComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'add-word',
    loadComponent: () => import('./features/add-word/add-word.component').then((c) => c.AddWordComponent),
    canActivate: [AuthGuardService],
  },
  {
    path: 'update-word/:id',
    loadComponent: () => import('./features/update-word/update-word.component').then((c) => c.UpdateWordComponent),
    canActivate: [AuthGuardService],
  },
  {
    path: 'flashcards',
    loadComponent: () => import('./features/flashcards/flashcards.component').then((c) => c.FlashcardsComponent),
    canActivate: [AuthGuardService],
  },
  {
    path: 'dialogs',
    loadComponent: () => import('./features/dialogs/dialogs.component').then((c) => c.DialogsComponent),
    canActivate: [AuthGuardService],
  },
  {
    path: '',
    component: ListWordsComponent,
    pathMatch: 'full',
    canActivate: [AuthGuardService],
  },
]
