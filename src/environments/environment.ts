// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDQ9fM6vF3Wd8qfmVv0hfeZP22H6ss9wJE',
    authDomain: 'learn-ukrainian-6df98.firebaseapp.com',
    databaseURL:
      'https://learn-ukrainian-6df98-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'learn-ukrainian-6df98',
    storageBucket: 'learn-ukrainian-6df98.appspot.com',
    messagingSenderId: '867290092809',
    appId: '1:867290092809:web:294d23e19b3c934393efd8',
    measurementId: 'G-H6RTFG96FT',
  },
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
